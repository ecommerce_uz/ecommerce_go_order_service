package main

import (
	"context"
	"net"

	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/config"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/grpc"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/grpc/client"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/pkg/logger"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/storage/postgres"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug
	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	// connect to db
	pgStore, err := postgres.NewPostgres(context.Background(), cfg)
	if err != nil {
		log.Panic("postgres.NewPostgres", logger.Error(err))
	}
	defer pgStore.CloseDB()
	// connect to services
	svrcs, err := client.NewGrpcClients(cfg)
	if err != nil {
		log.Panic("client.NewGrpcClients", logger.Error(err))
	}
	// BloomRPC

	grpcServer := grpc.SetUpServer(cfg, log, pgStore, svrcs)

	// listen porttcsrvcp
	lis, err := net.Listen("tcp", cfg.OrderGRPCPort)
	if err != nil {
		log.Panic("net.listen", logger.Error(err))
	}

	log.Info("GRPC: Server being started... ", logger.String("port", cfg.OrderGRPCPort))

	err = grpcServer.Serve(lis)
	if err != nil {
		log.Panic("grpcServer.Serve", logger.Error(err))
	}
}
