package grpc

import (
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/config"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/genproto/user_service"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/grpc/client"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/grpc/service"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/pkg/logger"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceMangerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
