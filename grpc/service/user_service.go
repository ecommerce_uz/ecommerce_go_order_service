package service

import (
	"context"

	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/config"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/genproto/user_service"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/grpc/client"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/pkg/logger"
	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceMangerI
	*user_service.UnimplementedUserServiceServer
}

func NewUserService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceMangerI) *UserService {
	return &UserService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *UserService) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.User, err error) {
	i.log.Info("---------CreateUser---------", logger.Any("req", req))

	resp, err = i.strg.User().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateUser->User->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *UserService) GetById(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error) {
	i.log.Info("---------GetUserById---------", logger.Any("req", req))

	resp, err = i.strg.User().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserById->User->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

// func (i *UserService) GetById(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error) {
// 	i.log.Info("---------GetUserById---------", logger.Any("req", req))

// 	resp, err = i.strg.User().GetByID(ctx, req)
// 	if err != nil {
// 		i.log.Error("!!!GetUserById->User->GetByID", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return resp, nil
// }
