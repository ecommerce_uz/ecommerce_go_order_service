package storage

import (
	"context"

	"gitlab.com/ecommerce_uz/ecommerce_go_order_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
}

type UserRepoI interface {
	Create(context.Context, *user_service.CreateUser) (*user_service.User, error)
	GetByID(context.Context, *user_service.UserPrimaryKey) (*user_service.User, error)
	GetList(context.Context, *user_service.GetListUserRequest) (*user_service.GetListUserResponse, error)
	Update(context.Context, *user_service.UpdateUser) (*user_service.User, error)
	Delete(context.Context, *user_service.UserPrimaryKey) error
}
